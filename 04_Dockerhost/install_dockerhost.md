# Install Ubuntu Server

## Download ISO
[Ubuntu Server 18.04.1 LTS](https://www.ubuntu.com/download/server)

## Prepare a USB Stick
For example with Etcher
See: [https://www.balena.io/etcher/](https://www.balena.io/etcher/)

## Install Ubuntu with there parameters

### Keyboard Layout

* Switzerland

### Disk 

* User entire disk (no LVM)

### Credentials and Co.

Your name: BFH
Server name: docker01
username: bfh
password: {see OneNote} or choos your own

### Snaps

* Don't installa any Snaps!

## Install Docker Engine!

### Use Docker Install instruction

Proceed exactely as explained in this documentaion:

[https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce)

### Add your user to "Docker" Group 

This allows you to user docker commands without using "sudo":

[https://docs.docker.com/install/linux/linux-postinstall/](https://docs.docker.com/install/linux/linux-postinstall/)

### Enables Docker Swarm mode

Swarm mode give some nice features, like Docker Secrets.

Enable Swarm mode:

```bash
docker swarm init
```


### Deploy Portainer as Container Orchestrations 

Create a folder
```bash
/opt/dockerdata/portainer
```

`cd` into it
```bash
cd /opt/dockerdata/portainer
```

Downlaod the prepared Portainer stack-compose file
```bash
wget https://raw.githubusercontent.com/akeusen/BTI7252-Stackfiles/master/portainer/portainer-stack.yaml
```

Deploy Portainer as Docker-Swarm-Service
```bash
docker network create --driver overlay --attachable backend

docker stack deploy --compose-file portainer-stack.yaml portainer
```

Now you can access Portainer via [http://DOCKERHOST:9000](http://DOCKERHOST:9000)

### Deploy Traefik (http-router)

You need you own domain for this setup (only the TLS part)

Create a folder
```bash
/opt/dockerdata/traefik
```

`cd` into it
```bash
cd /opt/dockerdata/traefik
```

Create these files (and adjust permission)
```bash
touch acme.json
chmod 600 acme.json
touch traefik.log
touch access.log
```

* Login into your Portainer instance.
* Navigate to _Stacks_
* Click _Add Stack_
* add a _Name_ for your Stack
* Paste the [Traefik-YAML](https://github.com/akeusen/BTI7252-Stackfiles/blob/master/traefik/traefik-stack.yaml) File into the _Web Editor_
  * Attention: You need your own domain for this!
* Click _Deploy the Stack_

