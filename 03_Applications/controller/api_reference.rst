API Reference
=============
The API reference defines where requests can be sent to, what should be sent and what can be expected back.


/initialize 
-------------
At /initialize the client can force the controller to initialize all variables an functions e.g if the things inventory has changed.
then the initialize command is used to reload the id's.

+-------------------------------+------+----------------------------------------------------------------+
| Expects                       | Data | cURL example                                                   |
+===============================+======+================================================================+
| POST request                  | n.A. | curl -X POST http://localhost:1880/initialize                   |
+-------------------------------+------+----------------------------------------------------------------+

+-------------------------------+--------------------------------+--------------+
| Returns                       | Requirement                    | Data         |
+===============================+================================+==============+
| http code 200                 | on success                     | -            |
+-------------------------------+--------------------------------+--------------+
| http code 4xx                 | on error                       | -            |
+-------------------------------+--------------------------------+--------------+


/events/
-----------
The /events/ directive is used to send events to the middle layer.

+-----------------------------+------------+---------------------------------------------+
| Expects                     | Data       | cURL example                                |
+=============================+============+=============================================+
| POST data                   | Data event | curl -H "Content-Type: application/json"    |
|                             |            | -d event_data http://localhost:1880/events/ |
+-----------------------------+------------+---------------------------------------------+

**event_data**
::

    {  "event": 
        {
            "cid":  "xyz",
            "tid":  123,
            "name": "activate",
            "parameters": [
                    {
                        "name" : true
                    }
                ]
        }
    }


+-------------------------------+--------------------------------+--------------+
| Returns                       | Requirement                    | Data         |
+===============================+================================+==============+
| http code 200                 | on success                     | -            |
+-------------------------------+--------------------------------+--------------+
| http code 4xx                 | on error                       | -            |
+-------------------------------+--------------------------------+--------------+
