# Overview _(Draft)_

![arch_drawing](https://gitlab.com/ch.nexhome/documentation/raw/master/diagrams/architecture_overview_v1.0.png)

Source: BTI7252/diagrams/architecture_overview.xml

## Suggested team scopes

* Application (Logic & Widget)
* Basic Services like IAM, Event-Handler and so on...
* Cloud Storage and Fog-Device
* Sensor/Actor inkl. connection to Fog-Device