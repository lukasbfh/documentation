# Smart Home Commander API

This API provides the fuctions to register and update metadata of a thing.
You can also use the swagger documentation in the repository [[https://gitlab.com/ch.nexhome/smarthomecommander |https://gitlab.com/ch.nexhome/smarthomecommander ]]

## Register
* `POST {{apiurl}}/things/register`

### Payload
A thing payload has the following sections:
* `root` section for base data such as id, name etc.
* `data` section which describes the readable values of this thing 
* `event` section which describes the writable / executable events that can be triggered on the thing 

#### JSON Structure
```json
{
    "thingId" : "<uuid>",
    "description": "<string>",
    "created": "<date>",
    "updated": "<date>",

    "data": [
        {
            "name": "<string>",
            "valueType": "<string>", 
            "valueUnit": "<string>",
        }
    ],

    "events": [
        {
            "name": "<string>",
            "parameters": [
                {
                    "name": "<string>",
                    "valueType": "<string>",
                }
            ]
        }
    ]
}
```

### Register consequences
If a new thing is registered the API will provide the new thing in its list call and all defined events can be triggered. Whichs means for every event the following URL can be called:

* `GET {{apiurl}}/things/{{thingId}}`
Response payload:
```json
{
    ... sensor definition ...
}
```

* `PUT {{apiurl}}/things/{{thingId}}/{{event.name}}`
Request payload:
```json
{
    "parameters": [
    {
        "name": value
    }]
}
```

With data and event definitions the resulting MQTT topics for live data can be derived as follows:

* Data: `nexhome/data/{{thingId}}/{{data.name}}`
* Events: `nexhome/event/{{thingId}}/{{event.name}}`


## Get All Things
* `GET {{apiurl}}/things`

Returns a list of thing definitions. 

## Get One Thing
* `GET {{apiurl}}/things/{{thingId}}`

Returns a thing definition for the given thingId. 

## Examples
### Example register request paload for a Fridge
```json
{
    "thingId" : "38d61641-1475-452f-8ccd-a74ed59f31ca",
    "description": "Fridge in the kitchen",
    "created": "2018-04-23T18:25:43.511Z",
    "updated": "2018-04-27T18:25:43.511Z",

    "data": [
        {
            "name": "temp",
            "valueType": "double", 
            "valueUnit": "celsius",
        },
        {
            "name": "light",
            "valueType": "boolean", 
        },
        {
            "name": "running",
            "valueType": "boolean", 
        }
    ],

    "events": [
        {
            "name": "running",
            "parameters": [
                {
                    "name": "value",
                    "type": "boolean"
                }
            ]
        }
    ]

}
```

which results in the following API calls / MQTT Topics:

### API calls
* `GET {{apiurl}}/things/38d61641-1475-452f-8ccd-a74ed59f31ca`
* `PUT {{apiurl}}/things/38d61641-1475-452f-8ccd-a74ed59f31ca/running`

```json
{
    "parameters": [
    {
        "running": true
    }]
}
```

### MQTT topics
Subcriptions
* `nexhome/data/38d61641-1475-452f-8ccd-a74ed59f31ca/temp`
* `nexhome/data/38d61641-1475-452f-8ccd-a74ed59f31ca/light`
* `nexhome/data/38d61641-1475-452f-8ccd-a74ed59f31ca/running`

Publishes
* `nexhome/event/38d61641-1475-452f-8ccd-a74ed59f31ca/running`
